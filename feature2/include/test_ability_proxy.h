#ifndef TEST_ABILITY_PROXY_H
#define TEST_ABILITY_PROXY_H

#include "if_testa_bility.h"
#include "iremote_proxy.h"
#include "iremote_object.h"

namespace OHOS {

class TestAbilityProxy : public IRemoteProxy<ITestAbility> {
public:
    explicit TestAbilityProxy(const sptr<IRemoteObject> &impl);
    int TestPingAbility(const std::u16string &dummy) override;
    int AddVolume(int volume);
private:
     static inline BrokerDelegator<TestAbilityProxy> delegator_; // 方便后续使用iface_cast宏
 };

}

#endif