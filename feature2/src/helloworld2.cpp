#include "if_testa_bility.h"
#include "string_ex.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <ostream>
#include "test_ability_proxy.h"
#include "iservice_registry.h"
#include "system_ability_definition.h"

#include "discovery_service.h"
#include "softbus_bus_center.h"
#include "session.h"

#include "ipc_skeleton.h"


using namespace OHOS;
static constexpr char DBINDER_PKG_NAME[] = "DBinderService";

int main(){
    auto sam = SystemAbilityManagerClient::GetInstance().GetSystemAbilityManager();
    if (sam == nullptr) {
        printf("GetSystemAbilityManager return nullptr\n");
        exit(EXIT_FAILURE);
    }

    // sptr<DBinderService> dBinderService = DBinderService::GetInstance();
    // if (dBinderService == nullptr) {
    //     printf("dBinderService is null");
    //     exit(EXIT_FAILURE);
    // }

    // NodeBasicInfo localNodeInfo;
    // GetLocalNodeDeviceInfo("aaa", &localNodeInfo);
    // printf("localNodeInfo.networkId %s\n", localNodeInfo.networkId);
    NodeBasicInfo *nodeInfo[4];
    int32_t infoNum = 4;
    GetAllNodeDeviceInfo(DBINDER_PKG_NAME, nodeInfo, &infoNum);

    static std::string g_deviceId = nodeInfo[0]->networkId;
    
    std::cout<<"remote g_deviceId: " << g_deviceId << std::endl;

    sptr<IRemoteObject> testAbilitySa = sam->GetSystemAbility(9998, g_deviceId);
    if (testAbilitySa == nullptr) {
        printf("GetSystemAbility return nullptr\n");
        exit(EXIT_FAILURE);
    }
    auto testAbility = iface_cast<ITestAbility>(testAbilitySa);

    auto g_proxy = reinterpret_cast<IPCObjectProxy *>(testAbilitySa.GetRefPtr());
    printf("IF_PROT_DATABUS is  %d %d\n", g_proxy->GetProto(), IRemoteObject::IF_PROT_DATABUS);

    int a = testAbility->AddVolume(123);
    std::cout << "TestPingAbility : "<< a << std::endl;
    return 0;
}

