#include "test_ability_proxy.h"

namespace OHOS {

 TestAbilityProxy::TestAbilityProxy(const sptr<IRemoteObject> &impl) : IRemoteProxy<ITestAbility>(impl)
 {
 }

 int TestAbilityProxy::TestPingAbility(const std::u16string &dummy){
    MessageOption option;
    MessageParcel dataParcel, replyParcel;
    if(!dataParcel.WriteInterfaceToken(GetDescriptor())) { // 所有对外接口的proxy实现都要写入接口描述符，用于stub端检验
        return -1;
    }
    if(!dataParcel.WriteString16(dummy)) {
        return -1;
    }
    int error = Remote()->SendRequest(TRANS_ID_PING_ABILITY, dataParcel, replyParcel, option);
    int result = (error == ERR_NONE) ? replyParcel.ReadInt32() : -1;
    return result;
 }

 int TestAbilityProxy::AddVolume(int volume){
    MessageOption option;
    MessageParcel dataParcel, replyParcel;
    if(!dataParcel.WriteInterfaceToken(GetDescriptor())) { // 所有对外接口的proxy实现都要写入接口描述符，用于stub端检验
        return -1;
    }
    if(!dataParcel.WriteInt32(volume)) {
        return -1;
    }
    int error = Remote()->SendRequest(ADD_VOLUME, dataParcel, replyParcel, option);
    int result = (error == ERR_NONE) ? replyParcel.ReadInt32() : -1;
    return result;
 } 

}