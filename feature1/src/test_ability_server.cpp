#include "test_ability_server.h"
#include "system_ability_definition.h"
#include "system_ability.h"
#include <cstdio>
#include <unistd.h>
#include "iservice_registry.h"


namespace OHOS {


REGISTER_SYSTEM_ABILITY_BY_ID(TestAbilityServer, 9998, true);

TestAbilityServer::TestAbilityServer(int32_t saId, bool runOnCreate) : SystemAbility(saId, runOnCreate)
{
}

int TestAbilityServer::TestPingAbility(const std::u16string &dummy) {
    return 4444;
}

int TestAbilityServer::AddVolume(int volume)
{
    pid_t current = getpid();
    printf("ListenAbility::AddVolume volume = %d, pid = %d.", volume, current);
    return (volume + 1);
}



void TestAbilityServer::OnStart()
{
    // printf("TestAbilityServer OnStart called!\n");
    // if (!Publish(this)) {
    //     printf("SoftBusServer publish failed!\n");
    // }    

    // auto samgr = SystemAbilityManagerClient::GetInstance().GetSystemAbilityManager();
    // ISystemAbilityManager::SAExtraProp saExtra;
    // saExtra.isDistributed = true; // 设置为分布式SA
    // int result = samgr->AddSystemAbility(TEST_ABILITY_ID, this, saExtra);
    // printf("123\n");


    printf("TestAbilityServer::OnStart()");
    printf("TestAbilityServer:%s called:-----Publish------", __func__);
    bool res = Publish(this);
    if (res) {
        printf( "TestAbilityServer: res == false");
    }
    printf("TestAbilityServer:%s called:AddAbilityListener_OS_TST----beg-----", __func__);
    AddSystemAbilityListener(DISTRIBUTED_SCHED_TEST_OS_ID);
    printf("TestAbilityServer:%s called:AddAbilityListener_OS_TST----end-----", __func__);

    printf("TestAbilityServer:%s called:StopAbility_OS_TST----beg-----", __func__);
    StopAbility(DISTRIBUTED_SCHED_TEST_OS_ID);
    printf("TestAbilityServer:%s called:StopAbility_OS_TST----end-----", __func__);
    return;

}

}