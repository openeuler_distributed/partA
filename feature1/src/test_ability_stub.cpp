
#include "test_ability_stub.h"
#include "system_ability_definition.h"
#include "system_ability.h"
#include <cstdio>

namespace OHOS {


int TestAbilityStub::OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option)
{
    if (data.ReadInterfaceToken() != GetDescriptor()) { // 校验是否为本服务的接口描述符，避免中继攻击
        return -1;
    }
    switch (code) {
        case TRANS_ID_PING_ABILITY: {
            std::u16string dummy = data.ReadString16();
            int result = TestPingAbility(dummy);
            reply.WriteInt32(result);
            return 0;
        }
        case ADD_VOLUME: {
            int dummy = data.ReadInt32();
            int result = AddVolume(dummy);
            reply.WriteInt32(result);
            return 0;            
        }
        default:
            return IPCObjectStub::OnRemoteRequest(code, data, reply, option);
    }
}
}