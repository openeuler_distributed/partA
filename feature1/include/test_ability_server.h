#ifndef TEST_ABILITY_SERVER_H
#define TEST_ABILITY_SERVER_H

#include "test_ability_stub.h"
#include "system_ability.h"
#include "singleton.h"

namespace OHOS {

class TestAbilityServer : public SystemAbility, public TestAbilityStub {
    // DECLARE_DELAYED_SINGLETON(TestAbilityServer);
    DECLARE_SYSTEM_ABILITY(TestAbilityServer);

public:
    TestAbilityServer(int32_t saId, bool runOnCreate);
    ~TestAbilityServer() = default;


    int TestPingAbility(const std::u16string &dummy) override;
    int AddVolume(int volume) override;

protected:
    void OnStart() override;
    // void OnStop() override;

};

}

#endif