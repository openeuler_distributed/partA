#ifndef IF_TESTA_BILITY_H
#define IF_TESTA_BILITY_H

#include "iremote_broker.h"
namespace OHOS {

class ITestAbility : public IRemoteBroker {
public:
  enum {
    TRANS_ID_PING_ABILITY = 1,
    ADD_VOLUME = 2
  };

  // 必须的，用于iface_cast
  DECLARE_INTERFACE_DESCRIPTOR(u"OHOS.test.ITestAbilityaaa");
  // 定义业务函数
  virtual int TestPingAbility(const std::u16string &dummy) = 0;
  virtual int AddVolume(int volume) = 0;
};

} // namespace OHOS
#endif