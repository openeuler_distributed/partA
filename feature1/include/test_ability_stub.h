#ifndef TEST_ABILITY_STUB_H
#define TEST_ABILITY_STUB_H

#include "if_testa_bility.h"
#include "iremote_stub.h"

namespace OHOS {
class TestAbilityStub : public IRemoteStub<ITestAbility> {
public:
    virtual int OnRemoteRequest(uint32_t code, MessageParcel &data, MessageParcel &reply, MessageOption &option) override;
};
}
#endif